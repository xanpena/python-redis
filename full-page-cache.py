import redis

r = redis.StrictRedis() # r = redis.StrictRedis(db=8)

content = """
 <html>
    <head><title>Código HTML</title></head>
    <body>
        <h1>Página en cache</h1>
        <span id="price">19.90 €</span>
    </body>
 </html>
"""
# podriamos tener páginas geolocalizadas cacheadas
r.set('price_es', content)
# r.setex(name='pricing_mx', value=content, time=36000) este contenido se borrará transcurrido este tiempo
r.get('price_es')